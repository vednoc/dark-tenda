// ==UserScript==
// @name         Responsive design for Tenda
// @namespace    https://gitlab.com/vednoc/dark-tenda
// @version      0.1.0
// @description  Make the UI scale properly for responsive design.
// @author       vednoc
// @match        http://192.168.0.1/login.asp
// @match        http://192.168.0.1/index.asp
// @match        http://192.168.0.1/advance.asp
// @grant        none
// ==/UserScript==

(function() {
  'use strict';
  var metaTag=document.createElement('meta');
  metaTag.name = "viewport"
  metaTag.content = "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
  document.getElementsByTagName('head')[0].appendChild(metaTag);
})();
